//
//  CountryList.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

public final class CountryList: Decodable {
    public let countries: [Country]

    public init(countries: [Country]) {
        self.countries = countries
    }

    private enum CodingKeys: String, CodingKey {
        case countries = "countryList"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        self.countries = try values.decode([Country].self, forKey: .countries)
    }
}
