//
//  NetworkManager.swift
//  MVVM
//
//  Created by hripsimem on 11/30/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Combine

final class NetworkManager {

    init() {}

    func fetch<T: Decodable, R: Routing>(_ routing: R) -> AnyPublisher<T, Error> {
        let urlSession = URLSession(configuration: .default)

        guard let url = routing.urlRequest else {
            fatalError("Could not create url")
        }

        return urlSession.dataTaskPublisher(for: url)
            .mapError { $0 as Error }
            .map { $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
