//
//  String+Extension.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/9/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, comment: "")
    }
}
