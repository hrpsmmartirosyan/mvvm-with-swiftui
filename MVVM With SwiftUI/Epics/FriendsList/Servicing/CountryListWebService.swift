//
//  CountryListWebService.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Combine

final class CountryListWebService: DataFetchingManager {
    func call(with parameters: CountryListParameters) -> AnyPublisher<CountryList, Error> {
        return self.execute(parameters, errorType: Error.self)
    }
}
