//
//  CountryView.swift
//  MVVM
//
//  Created by hripsimem on 12/11/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import SwiftUI

public struct CountryCellPresentationModel: Hashable {
    let name: String
    let description: String
}

struct CountryView: View {
    private let country: CountryCellPresentationModel
    
    init(country: CountryCellPresentationModel) {
        self.country = country
    }

    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 15) {
                Text(country.name)
                    .font(.system(size: 18))
                    .foregroundColor(Color.blue)
                Text(country.description)
                    .font(.system(size: 14))
            }
        }
    }
}

