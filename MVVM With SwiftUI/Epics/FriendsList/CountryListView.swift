//
//  CountryListView.swift
//  MVVM With SwiftUI
//
//  Created by hripsimem on 12/12/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import SwiftUI

struct CountryListView: View {
    @ObservedObject var viewModel = CountryListViewModel()

    var body: some View {
        NavigationView {
            List(self.viewModel.countries, id: \.self) {
                CountryView(country: $0)
            }.navigationBarTitle("country.list.title".localized)
                .onAppear {
                    self.viewModel.fetchCountryList()
            }

            .alert(isPresented: $viewModel.showErrorMessage) {
                Alert(title: Text("network.error.title".localized), message: Text("network.error.message".localized), dismissButton: .default(Text("error.message.ok.title".localized)))
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CountryListView()
    }
}


