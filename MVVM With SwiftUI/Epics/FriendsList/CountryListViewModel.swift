//
//  CountryListViewModel.swift
//  MVVM
//
//  Created by hripsimem on 12/11/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Combine

class CountryListViewModel: ObservableObject {
    @Published var countries: [CountryCellPresentationModel] = []
    @Published var showErrorMessage: Bool = false
    @Published var shouldAnimate = true

    // MARK: Private members
    private let webService = CountryListWebService(with: NetworkManager())
    private var cancellable = Set<AnyCancellable>()

    //MARK: Public API
    func fetchCountryList() {
        let parameters = CountryListParameters()

        webService
            .call(with: parameters)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case .finished:
                    break
                case .failure(_):
                    self.showErrorMessage = true
                }
            }, receiveValue: { [weak self] (list) in
                self?.shouldAnimate = false
                let countries = list.countries.map { CountryCellPresentationModel(name: $0.name, description: $0.description) }
                self?.countries = countries
            }).store(in: &cancellable)
    }
}
